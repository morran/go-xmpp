package xmpp

import "encoding/xml"

type ping struct {
	XMLName xml.Name `xml:"ping"`
	XMLns   string   `xml:"xmlns,attr"`
}

// PingC2S ...
func (c *Client) PingC2S(jid, server string) (int, error) {
	if jid == "" {
		jid = c.jid
	}
	if server == "" {
		server = c.domain
	}

	return c.SendStanza(IQStanza{
		Stanza: Stanza{
			From: xmlEscape(jid),
			To:   xmlEscape(server),
			Type: IQTypeGet,
			ID:   c.ID(),
		},
		Body: []interface{}{
			ping{
				XMLns: "urn:xmpp:ping",
			},
		},
	})
}

// PingS2S ...
func (c *Client) PingS2S(fromServer, toServer string) (int, error) {
	return c.SendStanza(IQStanza{
		Stanza: Stanza{
			From: xmlEscape(fromServer),
			To:   xmlEscape(toServer),
			Type: IQTypeGet,
			ID:   c.ID(),
		},
		Body: []interface{}{
			ping{
				XMLns: "urn:xmpp:ping",
			},
		},
	})
}

// SendResultPing ...
func (c *Client) SendResultPing(id, toServer string) (int, error) {
	return c.SendStanza(IQStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   xmlEscape(toServer),
			Type: IQTypeResult,
			ID:   id,
		},
	})
}
