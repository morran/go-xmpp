package xmpp

import "encoding/xml"

type PresenceStanza struct {
	XMLName xml.Name `xml:"presence"`
	Stanza

	Lang   string      `xml:"xml:lang,attr,omitempty"`
	Show   string      `xml:"show,omitempty"`
	Status string      `xml:"status,omitempty"`
	Body   interface{} `xml:",any,omitempty"`
}

// Presence is an XMPP presence notification.
type Presence struct {
	From   string
	To     string
	Type   string
	Show   string
	Status string
	X      []X
}

type clientPresence struct {
	XMLName xml.Name `xml:"jabber:client presence"`
	Stanza

	Lang     string `xml:"lang,attr"`
	Show     string `xml:"show"`   // away, chat, dnd, xa
	Status   string `xml:"status"` // sb []clientText
	Priority string `xml:"priority,attr"`
	X        []X    `xml:"x"`
	Error    *clientError
}
