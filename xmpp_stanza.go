package xmpp

import (
	"encoding/xml"
	"fmt"
)

type Stanza struct {
	From string `xml:"from,attr,omitempty"`
	To   string `xml:"to,attr,omitempty"`
	ID   string `xml:"id,attr,omitempty"`
	Type string `xml:"type,attr,omitempty"`
}

// SendStanza sends an information query request to the server.
// func (c *Client) SendStanza(to, iqType, requestNamespace, body string) (int, error) {
// 	id := strconv.FormatUint(uint64(getCookie()), 10)
// 	const xmlIQ = "<iq from='%s' to='%s' id='%s' type='%s'><query xmlns='%s'>%s</query></iq>"
// 	return fmt.Fprintf(c.conn, xmlIQ, xmlEscape(c.jid), xmlEscape(to), id, iqType, requestNamespace, body)
// 	// add generation id
// 	// use getCookie for a pseudo random id.
// }
func (c *Client) SendStanza(input interface{}) (int, error) {
	res, err := xml.Marshal(input)
	if err != nil {
		return 0, err
	}
	fmt.Println(string(res))
	return fmt.Fprintf(c.conn, string(res))
}
