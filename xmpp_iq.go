package xmpp

import "encoding/xml"

// IQType Enum
const (
	IQTypeGet    = "get"
	IQTypeSet    = "set"
	IQTypeResult = "result"
)

type Query struct {
	XMLName xml.Name      `xml:"query"`
	XMLns   string        `xml:"xmlns,attr"`
	Body    []interface{} `xml:",any,omitempty"`
}

type session struct {
	XMLName xml.Name `xml:"session"`
	XMLns   string   `xml:"xmlns,attr"`
}

type bind struct {
	XMLName  xml.Name `xml:"bind"`
	XMLns    string   `xml:"xmlns,attr"`
	Resource string   `xml:"resource,omitempty"`
}

type IQStanza struct {
	XMLName xml.Name `xml:"iq"`
	Stanza
	Body []interface{} `xml:",any,omitempty"`
}

// IQ ...
type IQ struct {
	ID    string
	From  string
	To    string
	Type  string
	Query []byte
}

type clientIQ struct { // info/query
	XMLName xml.Name `xml:"jabber:client iq"`
	Stanza

	Query []byte `xml:",innerxml"`
	Error clientError
	Bind  bindBind
}
