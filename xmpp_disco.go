package xmpp

// Namespace
const discoNS = "http://jabber.org/protocol/disco#items"

// Discovery ...
func (c *Client) Discovery() (int, error) {
	return c.SendStanza(IQStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   c.domain,
			Type: IQTypeGet,
			ID:   c.ID(),
		},
		Body: []interface{}{
			Query{
				XMLns: discoNS,
			},
		},
	})
}
