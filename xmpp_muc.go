// Copyright 2013 Flo Lauber <dev@qatfy.at>.  All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package xmpp ...
// TODO(flo):
//   - support password protected MUC rooms
//   - cleanup signatures of join/leave functions
package xmpp

import (
	"encoding/xml"
	"errors"
	"fmt"
	"strconv"
	"time"
)

const (
	nsMUC      = "http://jabber.org/protocol/muc"
	nsMUCUser  = "http://jabber.org/protocol/muc#user"
	nsMUCAdmin = "http://jabber.org/protocol/muc#admin"
)

const (
	// NoHistory ...
	NoHistory = 0
	// CharHistory ...
	CharHistory = 1
	// StanzaHistory ...
	StanzaHistory = 2
	// SecondsHistory ...
	SecondsHistory = 3
	// SinceHistory ...
	SinceHistory = 4
)

// X - x element from presence
type X struct {
	XMLName  xml.Name      `xml:"x"`
	XMLns    string        `xml:"xmlns,attr"`
	Item     Item          `xml:"item"`
	Password string        `xml:"password,omitempty"`
	Body     []interface{} `xml:",any,omitempty"`
}

type History struct {
	XMLName    xml.Name `xml:"history"`
	MaxChars   string   `xml:"maxchars,attr,omitempty"`
	MaxStanzas string   `xml:"maxstanzas,attr,omitempty"`
	Seconds    string   `xml:"seconds,attr,omitempty"`
	Since      string   `xml:"since,attr,omitempty"`
}

// Item - item element from x in received presence
type Item struct {
	XMLName     xml.Name `xml:"item"`
	Nick        string   `xml:"nick,attr,omitempty"`
	Jid         string   `xml:"jid,attr,omitempty"`
	Affiliation string   `xml:"affiliation,attr,omitempty"`
	Role        string   `xml:"role,attr,omitempty"`
}

// SendTopic send sends room topic wrapped inside an XMPP message stanza body.
func (c *Client) SendTopic(chat Chat) (n int, err error) {
	return c.SendStanza(MessageStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   xmlEscape(chat.Remote),
			ID:   c.ID(),
			Type: xmlEscape(chat.Type),
		},
		Lang:    "en",
		Subject: xmlEscape(chat.Text),
	})
}

// JoinMUC ...
// xep-0045 7.2
func (c *Client) JoinMUC(jid, nick string, password string, historyType, history int, historyDate *time.Time) (n int, err error) {
	if nick == "" {
		nick = c.jid
	}

	hist := History{}

	switch historyType {
	case NoHistory:
		hist.MaxChars = "0"
	case CharHistory:
		hist.MaxChars = strconv.Itoa(history)
		break
	case StanzaHistory:
		hist.MaxStanzas = strconv.Itoa(history)
		break
	case SecondsHistory:
		hist.Seconds = strconv.Itoa(history)
		break
	case SinceHistory:
		if historyDate != nil {
			hist.Seconds = historyDate.Format(time.RFC3339)
		}
		break
	default:
		return 0, errors.New("Unknown history option")
	}

	body := X{XMLns: nsMUC}

	if password != "" {
		body.Password = xmlEscape(password)
	}

	if hist != (History{}) {
		body.Body = append(body.Body, hist)
	}
	return c.SendStanza(PresenceStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   fmt.Sprintf("%s/%s", xmlEscape(jid), xmlEscape(nick)),
			ID:   c.ID(),
		},
		Body: []interface{}{body},
	})
}

// JoinProtectedMUC ...
func (c *Client) JoinProtectedMUC(jid, nick string, password string, historyType, history int, historyDate *time.Time) (n int, err error) {
	return c.JoinMUC(jid, nick, password, NoHistory, 0, nil)
}

// JoinMUCNoHistory ...
func (c *Client) JoinMUCNoHistory(jid, nick string) (n int, err error) {
	return c.JoinMUC(jid, nick, "", NoHistory, 0, nil)
}

// LeaveMUC ...
// xep-0045 7.14
func (c *Client) LeaveMUC(jid string) (n int, err error) {
	return c.SendStanza(PresenceStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   xmlEscape(jid),
			Type: "unavailable",
		},
	})
}

// SetRole ...
func (c *Client) SetRole(role, nick, mucjid string) (int, error) {
	return c.SendStanza(IQStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   mucjid,
			ID:   c.ID(),
			Type: IQTypeSet,
		},
		Body: []interface{}{
			Query{
				XMLns: nsMUCAdmin,
				Body: []interface{}{
					Item{
						Nick: nick,
						Role: role,
					},
				},
			},
		},
	})
}

// SetAffiliation ...
func (c *Client) SetAffiliation(affiliation, nick, mucjid string) (int, error) {
	return c.SendStanza(IQStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   mucjid,
			ID:   c.ID(),
			Type: IQTypeSet,
		},
		Body: []interface{}{
			Query{
				XMLns: nsMUCAdmin,
				Body: []interface{}{
					Item{
						Nick:        nick,
						Affiliation: affiliation,
					},
				},
			},
		},
	})
}
