package xmpp

import "encoding/xml"

type MessageStanza struct {
	XMLName xml.Name `xml:"message"`
	Stanza

	Lang    string `xml:"xml:lang,attr,omitempty"`
	Body    string `xml:"body,omitempty"`
	Subject string `xml:"subject,omitempty"`
}

// RFC 3921  B.1  jabber:client
type clientMessage struct {
	XMLName xml.Name `xml:"jabber:client message"`
	Stanza

	// These should technically be []clientText, but string is much more convenient.
	Subject string `xml:"subject"`
	Body    string `xml:"body"`
	Thread  string `xml:"thread"`

	// Any hasn't matched element
	Other []XMLElement `xml:",any"`

	Delay Delay `xml:"delay"`
}
