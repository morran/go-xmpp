package xmpp

// ApproveSubscription ...
func (c *Client) ApproveSubscription(jid string) (int, error) {
	return c.SendStanza(PresenceStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   xmlEscape(jid),
			ID:   c.ID(),
			Type: "subscribed",
		},
	})
}

// RevokeSubscription ...
func (c *Client) RevokeSubscription(jid string) (int, error) {
	return c.SendStanza(PresenceStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   xmlEscape(jid),
			ID:   c.ID(),
			Type: "unsubscribed",
		},
	})
}

// RequestSubscription ...
func (c *Client) RequestSubscription(jid string) (int, error) {
	return c.SendStanza(PresenceStanza{
		Stanza: Stanza{
			From: c.jid,
			To:   xmlEscape(jid),
			ID:   c.ID(),
			Type: "subscribe",
		},
	})
}
